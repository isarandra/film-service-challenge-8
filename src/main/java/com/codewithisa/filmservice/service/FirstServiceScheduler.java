package com.codewithisa.filmservice.service;

import org.springframework.stereotype.Service;

@Service
public interface FirstServiceScheduler {
    void firstMethod();
}
